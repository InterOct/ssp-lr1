﻿using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Web;

namespace LR1
{
    public class Handler : IHttpHandler
    {
        private const string Base = @"C:\SSP\LR1\LR1\";
        private const string DatabasePath = Base + "Database.mdb";
        private const string TemplatePath = Base + "template.html";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";

            var connection = new OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;data source=" + DatabasePath);
            connection.Open();

            var command = connection.CreateCommand();
            command.Connection = connection;
            command.CommandText = "SELECT * FROM People";

            var dataset = new DataSet();
            var dataAdapter = new OleDbDataAdapter(command);
            dataAdapter.Fill(dataset, "People");
            connection.Close();

            var content =new StringBuilder("<table class=\"table\"><tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Age</th></tr>");
            foreach (DataRow row in dataset.Tables["People"].Rows)
            {
                content.Append($"<tr><td>{row["ID"]}</td><td>{row["FirstName"]}</td><td>{row["LastName"]}</td><td>{row["Age"]}</td></tr>");
            }
            content.Append("</table>");
            
            context.Response.Write(
                File.OpenText(TemplatePath)
                    .ReadToEnd()
                    .Replace("{{title}}", DatabasePath)
                    .Replace("{{content}}", content.ToString())
            );
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}